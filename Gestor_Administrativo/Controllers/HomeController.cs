﻿using Gestor_Administrativo.Models.Generales;
using Gestor_Administrativo.Utiles;
using Monibyte.Arquitectura.Web.Nucleo.Controlador;
using Monibyte.Arquitectura.Comun.Nucleo.Utilitarios;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Threading;
using Gestor_Administrativo.Integracion.ReportServer;
using Gestor_Administrativo.Integracion.Dto;
using Monibyte.Arquitectura.Comun.Nucleo;
using Monibyte.Arquitectura.Comun.Nucleo.Repositorio;
using System.Linq;
using Monibyte.Arquitectura.Comun.Nucleo.Sesion;
using System.Threading.Tasks;

namespace Gestor_Administrativo.Controllers
{
    public class HomeController : ControladorBase
    {
        private EmailHelper _emailHelper;
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ValidaCierres(string conection, string instanciaSQL)
        {
            using (SqlConnection conn = new SqlConnection(conection))
            {
                try
                {
                    conn.Open();

                    var _repTrama = new RepositorioTrama();
                    var statement = ReadXml.GetSqlStatement("SqlArchivosCierre");

                    var commd = new SqlCommand(statement, conn);
                    SqlDataReader data = commd.ExecuteReader();
                    var archivosCierre = new List<ArchivosCierre>();
                    while (data.Read())
                    {
                        var archivo = new ArchivosCierre();
                        archivo.IdCierre = (int)data["IdCierre"];
                        archivo.NombreProceso = data["NombreProceso"].ToString();
                        archivo.NombreArchivo = data["NombreArchivo"].ToString();
                        archivo.RutaArchivo = data["RutaArchivo"].ToString();
                        archivo.FecUltimaCarga = data["FecUltimaCarga"] as DateTime?;
                        archivosCierre.Add(archivo);
                    }
                    InfoSesion.IncluirSesion(string.Concat(instanciaSQL, "Cierres"), archivosCierre);

                    var cmder = new SqlCommand(@"General.P_ValidaCierresSistemas", conn);
                    cmder.CommandType = System.Data.CommandType.StoredProcedure;

                    var reader = cmder.ExecuteReader();
                    var lista = new List<CierreSistema>();

                    while (reader.Read())
                    {
                        var cierre = new CierreSistema();
                        DateTime? horaUltEjecucion = reader["HoraUltEjecucion"] as DateTime?;
                        var idCierre = (int)reader["IdCierre"];
                        cierre.HoraUltEjecucion = horaUltEjecucion;
                        cierre.Descripcion = (string)reader["Descripcion"];
                        cierre.HoraEjecucion = (TimeSpan)(reader["HoraEjecucion"]);
                        cierre.DesEstadoEjecucion = (string)reader["DesEstadoEjecucion"];
                        cierre.IdEstadoEjecucion = (int)reader["IdEstadoEjecucion"];
                        if (!reader["FecInicio"].ToString().Equals(""))
                            cierre.FecInicio = (DateTime)reader["FecInicio"];
                        if (!reader["FecFinaliza"].ToString().Equals(""))
                            cierre.FecFinaliza = (DateTime)reader["FecFinaliza"];

                        var existArchivos = archivosCierre.Where(x => x.IdCierre == idCierre);
                        if (existArchivos.Any())
                        {
                            cierre.AplicaArchivo = true;
                            cierre.NoCargado = existArchivos.Any(x =>
                                x.FecUltimaCarga <= DateTime.Now.Date || x.FecUltimaCarga == null);
                        }
                        else
                        {
                            cierre.AplicaArchivo = false;
                            cierre.NoCargado = false;
                        }

                        lista.Add(cierre);
                    }

                    ViewBag.Model = lista;
                    ViewBag.GridCierre = string.Concat(instanciaSQL, "Cierres");
                    conn.Close();
                    return PartialView("_DetalleCierre");
                }
                catch
                {
                    return null;
                }
            }
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult ValidaArchivos(string entidad, string idCierre)
        {
            try
            {
                var archivos = InfoSesion.ObtenerSesion<List<ArchivosCierre>>(entidad + "Cierres");
                ViewBag.Model = archivos;
                ViewBag.Entidad = entidad;
                ViewBag.IdCierre = idCierre;
                ViewBag.GridCierre = entidad + "Cierres";
                ViewBag.IsDialog = Request.IsAjaxRequest();
                if (Request.IsAjaxRequest())
                {
                    return PartialView("_DetalleArchivos");
                }
                return View("_DetalleArchivos");
            }
            catch
            {
                return null;
            }
        }

        [ChildActionOnly]
        public ActionResult ArchivoMaestro(string conection, string instanciaSQL)
        {
            var lista = new List<CantidadMovimientos>();

            using (SqlConnection conn = new SqlConnection(conection))
            {
                conn.Open();

                SqlCommand StatementMaestro = null;
                SqlCommand StatementMovimientos = null;
                SqlCommand StatementMovMonibyte = null;
                var StatementNoCargados = new SqlCommand(ReadXml.GetSqlStatement("SqlMovimientosNoCargados"), conn);


                if (instanciaSQL == "LFSCR")
                {
                    StatementMaestro = new SqlCommand(ReadXml.GetSqlStatement("SqlArchivoMaestroCR"), conn);
                    StatementMovimientos = new SqlCommand(ReadXml.GetSqlStatement("SqlMovimientosCargadosCR"), conn);
                    StatementMovMonibyte = new SqlCommand(ReadXml.GetSqlStatement("SqlMovimientosMonibyteCR"), conn);
                }
                else
                {
                    StatementMaestro = new SqlCommand(ReadXml.GetSqlStatement("SqlArchivoMaestro"), conn);
                    StatementMovimientos = new SqlCommand(ReadXml.GetSqlStatement("SqlMovimientosCargados"), conn);
                }

                var reader = StatementMaestro.ExecuteReader();
                reader.Read();
                lista.Add(new CantidadMovimientos
                {
                    CantMovArchivoMaestro = (int)reader[0]
                });
                reader.Close();

                reader = StatementMovimientos.ExecuteReader();
                reader.Read();
                lista[0].CantMovCargados = (int)reader[0];
                reader.Close();

                if (instanciaSQL == "LFSCR")
                {
                    reader = StatementMovMonibyte.ExecuteReader();
                    reader.Read();
                    lista[0].CantMovMonibyte = (int)reader[0];
                    reader.Close();
                }

                reader = StatementNoCargados.ExecuteReader();

                if (reader.Read())
                    lista[0].MovNoValidos = reader[0] != DBNull.Value ? (string)reader[0] : "";
                reader.Close();

                ViewBag.ModelMov = lista;
                ViewBag.Nombre = instanciaSQL;
                conn.Close();
                return PartialView("_CantMovArcMaestros");


            }
        }

        public bool ActualizaCierre(string conection, int idCierre)
        {
            var lista = new List<CantidadMovimientos>();

            using (SqlConnection conn = new SqlConnection(conection))
            {
                conn.Open();
                var idCierres = ObtenerDependecia(idCierre, conn);

                foreach(var id in idCierres.Reverse())
                {
                    SqlCommand Statement = null;
                    RepositorioTrama _repTrama = new RepositorioTrama();
                    var paramSql = new DbParamList();

                    var date = string.Format("{0}:{1}:{2}", DateTime.Now.TimeOfDay.Hours, "00", "00");

                    paramSql.AddParameter("horaEjecucion", paramType: typeof(string), value: date);
                    paramSql.AddParameter("idCierre", paramType: typeof(int), value: id);
                    Statement = new SqlCommand(ReadXml.GetSqlStatement("UpdateCierres"), conn);
                    Statement.Parameters.AddRange(_repTrama.ParamListToSql(paramSql.ParamList));
                    Statement.ExecuteNonQuery();
                }
                conn.Close();
            }
            Thread.Sleep(5000);
            return false;
        }

        public  int[] ObtenerDependecia(int id, SqlConnection conn)
        {
            SqlCommand Statement = null;
            var result = new List<int>();
            result.Add(id);
            RepositorioTrama _repTrama = new RepositorioTrama();
            var paramSql = new DbParamList();

            paramSql.AddParameter("idCierre", paramType: typeof(int), value: id);
            Statement = new SqlCommand(ReadXml.GetSqlStatement("SqlCierre"), conn);
            Statement.Parameters.AddRange(_repTrama.ParamListToSql(paramSql.ParamList));
            SqlDataReader reader = Statement.ExecuteReader();
            while (reader.Read())
            {
                var idCierre = string.IsNullOrEmpty(reader["IdDependeDe"].ToString()) ? (int?)null :
                            Convert.ToInt32(reader["IdDependeDe"]);
                if(idCierre != null)
                {
                    var idDep = ObtenerDependecia(idCierre.Value, conn);
                    if (idDep != null)
                    {
                        result.AddRange(idDep);
                    }
                }
            }
            return result.ToArray(); ;
        }

        public JsonResult EnviarCorreo(string Mensaje)
        {
            var configSender = new SenderConfig(Config.ReadProp<string>("SenderEmail"), null);
            Mensaje = Mensaje ?? "Todos los Cierres OK!!";
            var email = new EmailConfig("Revisión Diaria de Cierres MONIBYTE " + DateTime.Now.Date.ToShortDateString(), Mensaje);

            var reportes = new RepositorioReportServer("report-mb-connection", true);
            Reporte resultado = reportes.ExportPdf("/Reports/Notificaciones/RevisionCierres", null);
            email.AddAttachment(resultado.Output, string.Format("Cierres-{0}",
                DateTime.Now.Date.ToShortDateString()), resultado.MimeType);
            email.AddTo(Config.ReadProp<string>("DestinationEmail"));
            _emailHelper = _emailHelper != null ?
                _emailHelper : new EmailHelper("smtp-monibyte-cesand", web: true);

            ThreadPool.QueueUserWorkItem(state => _emailHelper.SendEmail(configSender, email));
            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}
