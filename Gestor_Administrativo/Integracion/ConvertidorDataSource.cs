﻿using Kendo.Mvc.UI;
using Monibyte.Arquitectura.Comun.Nucleo.Data;
using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;

namespace Gestor_Administrativo.Models.Generales
{
    public static class ConvertidorDataSource
    {
        public static DataRequest ToDataRequest(this DataSourceRequest request)
        {
            var json = request.ToJson(JsonSettings.Set03);
            json = json.Replace("Kendo.Mvc.UI.", "Monibyte.Arquitectura.Comun.Nucleo.Data.")
                .Replace("Kendo.Mvc.UI", "Monibyte.Arquitectura.Comun.Nucleo")
                .Replace("Kendo.Mvc.", "Monibyte.Arquitectura.Comun.Nucleo.Data.")
                .Replace("Kendo.Mvc", "Monibyte.Arquitectura.Comun.Nucleo")
                .Replace("DataSourceRequest", "DataRequest");
            return json.FromJson<DataRequest>(JsonSettings.Set03);
        }
    }
}
