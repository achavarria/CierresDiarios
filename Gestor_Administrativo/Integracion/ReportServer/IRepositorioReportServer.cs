﻿using Gestor_Administrativo.Integracion.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gestor_Administrativo.Integracion.ReportServer
{
    public interface IRepositorioReportServer
    {
        Reporte ExportPdf(string report, params ReportParam[] parameters);
        Reporte ExportExcel(string report, params ReportParam[] parameters);
        Reporte ExportCsv(string report, params ReportParam[] parameters);
        Reporte ExportXml(string report, params ReportParam[] parameters);
        Reporte ExportDefinition(byte[] report, string format,
            params ReportParam[] parameters);
        bool RemoteFileExists(string url);
        void UploadFile(string path, string fileName, byte[] bytes,
            string extention, string mimeType);
    }
}
