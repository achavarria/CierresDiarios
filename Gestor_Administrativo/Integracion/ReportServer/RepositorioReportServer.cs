﻿using Monibyte.Arquitectura.Comun.Nucleo.Encryption;
using Monibyte.Arquitectura.Comun.Nucleo.Extensiones;
using Monibyte.Arquitectura.Comun.Nucleo.Utilitarios;
using Gestor_Administrativo.ReportServer;
using System;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using Gestor_Administrativo.Integracion.Dto;
using System.Collections.Generic;

namespace Gestor_Administrativo.Integracion.ReportServer
{
    public class RepositorioReportServer : IRepositorioReportServer
    {
        private string _reportsUrl;
        DataSourceCredentials[] _dataSourceCredentials;

        public RepositorioReportServer(string sectionName, bool web = true)
        {
            var conf = ConfigHelper.GetConfig(string.Empty, web);
            var section = ConfigHelper.GetConfigSection(conf, "ConfiguracionesReportServer", sectionName);

            //var user = ConfigHelper.ObtieneValPropiedad<string>(section, "DataSourceUser");
            //var pass = ConfigHelper.ObtieneValPropiedad<string>(section, "DataSourcePass");
            var dsName = ConfigHelper.ObtieneValPropiedad<string>(section, "DataSourceName");
            _reportsUrl = ConfigHelper.ObtieneValPropiedad<string>(section, "ResourcesUrl");

            var dsNames = dsName.Split(',');

            for (int runs = 0; runs < dsNames.Length; runs++)
            {
                var data = new DataSourceCredentials
                {
                    DataSourceName = dsNames[runs]
                    //UserName = user.Decrypt(),
                    //Password = pass.Decrypt()
                };

                if (_dataSourceCredentials == null)
                {
                    _dataSourceCredentials = new DataSourceCredentials[dsNames.Length];
                }

                _dataSourceCredentials[runs] = data;
            }
        }

        public Reporte ExportPdf(string report, params ReportParam[] parameters)
        {
            return ExportFile(report, "PDF", parameters);
        }

        public Reporte ExportExcel(string report, params ReportParam[] parameters)
        {
            return ExportFile(report, "EXCEL", parameters);
        }

        public Reporte ExportCsv(string report, params ReportParam[] parameters)
        {
            return ExportFile(report, "CSV", parameters);
        }

        public Reporte ExportXml(string report, params ReportParam[] parameters)
        {
            return ExportFile(report, "XML", parameters);
        }

        public Reporte ExportDefinition(byte[] report, string format, params ReportParam[] parameters)
        {
            return ExportFile(report, format, parameters);
        }

        public bool RemoteFileExists(string url)
        {
            try
            {
                //Creating the HttpWebRequest
                HttpWebRequest request = WebRequest.Create(string.
                    Format("{0}?{1}", _reportsUrl, url)) as HttpWebRequest;
                //Setting the Request method HEAD, you can also use GET too.
                request.Method = "GET";//"HEAD";
                //Getting the Web Response.
                request.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                //request.AuthenticationLevel = System.Net.Security.AuthenticationLevel.;
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                //Returns TRUE if the Status code == 200
                return (response.StatusCode == HttpStatusCode.OK);
            }
            catch
            {
                //Any exception will returns false.
                return false;
            }
        }

        public void UploadFile(string path, string fileName, byte[] bytes, string extention, string mimeType)
        {

            var reportServerAdmUrl = _reportsUrl + "/ReportService2005.asmx";
            using (var webServiceProxy = new ReportServerAdm.ReportingService2005SoapClient
                (GetDefaultBinding(), new EndpointAddress(reportServerAdmUrl)))
            {
                webServiceProxy.ClientCredentials.Windows.ClientCredential =
                       System.Net.CredentialCache.DefaultNetworkCredentials;
                webServiceProxy.ClientCredentials.Windows.AllowedImpersonationLevel =
                    System.Security.Principal.TokenImpersonationLevel.Impersonation;
                // Define las propiedades del recurso/archivo
                var properties = new ReportServerAdm.Property[]{
                    new ReportServerAdm.Property {
                        Name = mimeType,
                        Value = extention
                    }
                };
                // publica el archivo
                webServiceProxy.CreateResource(null, fileName,
                    path, true, bytes, mimeType, properties);
            }
        }

        internal Reporte ExportFile(object report, string format, params ReportParam[] parameters)
        {
            ReportParam[] newParams = null;
            if (parameters != null && parameters.Length > 0)
            {
                var length = parameters.Length;
                newParams = new ReportParam[length];
                for (int i = 0; i < parameters.Length; i++)
                {
                    newParams[i] = parameters[i];
                }

                //var exist = parameters.Any(x => x.Name == "IdIdioma");
                //if (!exist)
                //{
                //    newParams[length - 2] = new ReportParam { Label = "=Parameters!IdIdioma.Value", Name = "IdIdioma", Value = 1.ToString() };
                //}
                //exist = parameters.Any(x => x.Name == "IdIdiomaBase");
                //if (!exist)
                //{
                //    newParams[length - 1] = new ReportParam { Label = "=Parameters!IdIdiomaBase.Value", Name = "IdIdiomaBase", Value = 1.ToString() };
                //}
            }
            else
            {
                //newParams = new ReportParam[2];
                //newParams[0] = new ReportParam { Label = null, Name = "IdIdioma", Value = 1.ToString() };
                //newParams[1] = new ReportParam { Label = null, Name = "IdIdiomaBase", Value = 1.ToString() };
            }
            parameters = newParams;

            var reportServerUrl = _reportsUrl + "/ReportExecution2005.asmx";
            using (var webServiceProxy = new ReportExecutionServiceSoapClient
                (GetDefaultBinding(), new EndpointAddress(reportServerUrl)))
            {
                webServiceProxy.ClientCredentials.Windows.ClientCredential =
                       System.Net.CredentialCache.DefaultNetworkCredentials;
                webServiceProxy.ClientCredentials.Windows.AllowedImpersonationLevel =
                    System.Security.Principal.TokenImpersonationLevel.Delegation;
                // Init Report to execute
                Warning[] warnings;
                ExecutionInfo executionInfo;
                ServerInfoHeader serverInfoHeader;
                ExecutionHeader executionHeader = null;
                try
                {
                    if (report is string)
                    {
                        var _report = report as string;
                        executionHeader = webServiceProxy.LoadReport(null, _report, null,
                            out serverInfoHeader, out executionInfo);
                    }
                    else if (report is byte[])
                    {
                        var _report = report as byte[];
                        executionHeader = webServiceProxy.LoadReportDefinition(null, _report,
                            out serverInfoHeader, out executionInfo, out warnings);
                    }
                    ParameterValue[] paramValues = null;
                    if (parameters != null && parameters.Length > 0)
                    {
                        paramValues = parameters.ToJson()
                            .FromJson<ParameterValue[]>();
                    }
                    else
                    {
                        paramValues = new ParameterValue[0];
                    }
                    //Attach Report Parameters
                    webServiceProxy.SetExecutionParameters(executionHeader, null,
                        paramValues, null, out executionInfo);
                    //Set Credentials
                    //var credentials = _dataSourceCredentials.ToArray();
                    //webServiceProxy.SetExecutionCredentials(executionHeader,
                    //    null, credentials, out executionInfo);
                    // Render
                    byte[] output;
                    string[] streamIds;
                    string extension, mimeType, encoding;
                    webServiceProxy.Render(executionHeader, null, format, null, out output,
                        out extension, out mimeType, out encoding, out warnings, out streamIds);
                    return new Reporte
                    {
                        Output = output,
                        Encoding = encoding,
                        MimeType = mimeType,
                        Extension = extension
                    };
                }
                catch (FaultException faultExc)
                {
                    var msj = "Error generando el reporte: '{0}', Mensage: '{1}'";
                    throw new Exception(string.Format(msj, report, faultExc.Message), faultExc);
                }
            }
        }

        internal Binding GetDefaultBinding()
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxBufferPoolSize = 0;
            binding.MaxReceivedMessageSize = 2147483647;
            binding.HostNameComparisonMode = HostNameComparisonMode.StrongWildcard;
            binding.TextEncoding = System.Text.Encoding.UTF8;
            binding.MessageEncoding = WSMessageEncoding.Text;
            binding.TransferMode = TransferMode.Buffered;
            binding.UseDefaultWebProxy = true;
            binding.OpenTimeout = new TimeSpan(0, 10, 0);
            binding.CloseTimeout = new TimeSpan(0, 10, 0);
            binding.SendTimeout = new TimeSpan(0, 10, 0);
            binding.ReceiveTimeout = new TimeSpan(0, 59, 0);
            binding.ReaderQuotas = new System.Xml.XmlDictionaryReaderQuotas
            {
                MaxArrayLength = 2147483647,
                MaxBytesPerRead = 2147483647,
                MaxDepth = 2147483647,
                MaxNameTableCharCount = 2147483647,
                MaxStringContentLength = 2147483647
            };
            binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Ntlm;
            return binding;
        }
    }
}