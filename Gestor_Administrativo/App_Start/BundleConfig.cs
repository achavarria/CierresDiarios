﻿using System.Web;
using System.Web.Optimization;

namespace Gestor_Administrativo
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.IgnoreList.Clear();
            bundles.IgnoreList.Ignore("*.intellisense.js");
            bundles.IgnoreList.Ignore("*-vsdoc.js");
            bundles.IgnoreList.Ignore("*.debug.js", OptimizationMode.WhenEnabled);
            BundleTable.EnableOptimizations = false;
            /*****************************bundles comunes*****************************/
            var jqueryBundle = new ScriptBundle("~/Scripts/jquery")
                .Include("~/scripts/jquery-{version}.js",
                "~/scripts/bootstrap.js",
                "~/scripts/jquery.unobtrusive*",
                "~/Scripts/kendo.all.min.js",
                "~/Scripts/kendo.aspnetmvc.min.js",
                "~/scripts/jquery.blockui.js",
                "~/scripts/monibyte.utiles.comun.js",
                "~/scripts/monibyte.utiles.jquery.ui.js");
            jqueryBundle.Transforms.Add(new JsMinify());
            bundles.Add(jqueryBundle);
            //Validador por medio de jquery.validate
            var validateBundle = new ScriptBundle("~/Scripts/validate").Include(
                "~/scripts/jquery.validate*",
                "~/scripts/monibyte.utiles.globalize.js");
            validateBundle.Transforms.Add(new JsMinify());
            bundles.Add(validateBundle);
            bundles.Add(new ScriptBundle("~/scripts/modernizr")
                .Include("~/scripts/modernizr-*"));
            //Estilos monibyte
            var monibyteCssBundle = new StyleBundle("~/content/css")
                .Include(
                "~/Content/kendoui/kendo.common.core.min.css",
                "~/Content/kendoui/kendo.common.min.css",
                "~/Content/kendoui/kendo.default.min.css",
                "~/content/bootstrap.css",
                "~/Content/monibyte.css");
            monibyteCssBundle.Transforms.Add(new CssMinify());
            bundles.Add(monibyteCssBundle);
        }
    }
}
