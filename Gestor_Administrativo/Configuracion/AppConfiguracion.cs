﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace Gestor_Administrativo.Configuracion
{

    public class AppConfiguracion
    {
        private static AppConfiguracion instance;     

        public static AppConfiguracion Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new AppConfiguracion();
                }
                return instance;
            }
        }

        private AppConfiguracion()
        {
            ConnectionString = ConfigurationManager.ConnectionStrings["MonibyteConn"].ConnectionString;
            ConnectionStringMnb = ConfigurationManager.ConnectionStrings["MonibyteConnMnb"].ConnectionString;

            InstaciaSQL = ConfigurationManager.AppSettings["InstaciaSQL"];
            
        }

        private static string ObtenerValor(string llave)
        {
            return ConfigurationManager.AppSettings[llave];
        }

        public string ConnectionString { get; private set; }
        public string ConnectionStringMnb { get; private set; }

        public string InstaciaSQL { get; private set; }


    }

}