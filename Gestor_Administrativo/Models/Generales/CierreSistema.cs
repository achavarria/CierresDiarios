﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace Gestor_Administrativo.Models.Generales
{
    public class CierreSistema
    {
        public int IdCierre { get; set; }
        public int IdSistema { get; set; }
        public string Descripcion { get; set; }
        public int IdEstado { get; set; }
        public int OrdenEjecucion { get; set; }
        public TimeSpan HoraEjecucion { get; set; }
        public int IdDependeDe { get; set; }
        public string EjecutarMetodo { get; set; }
        public DateTime FecUltCierre { get; set; }
        public int IdEstadoEjecucion { get; set; }
        public int IdPeriodicidad { get; set; }
        public int ValPeriodicidad { get; set; }
        public DateTime? HoraUltEjecucion { get; set; }
        public DateTime FecInclusionAud { get; set; }
        public int IdUsuarioIncluyeAud { get; set; }
        public DateTime FecActualizaAud { get; set; }
        public int IdUsuarioActualizaAud { get; set; }
        public DateTime? FecInicio { get; set; }
        public DateTime? FecFinaliza { get; set; }
        public string DesEstadoEjecucion { get; set; }
        public bool AplicaArchivo { get; set; }
        public bool NoCargado { get; set; }
    }

    public class CantidadMovimientos
    {
        public int CantMovArchivoMaestro { get; set; }
        public int CantMovCargados { get; set; }
        public int CantMovMonibyte { get; set; }
        public string MovNoValidos { get; set; } 
    }

    public class Notificacion
    {
        public byte[] PDF { get; set; }
        public string Mensaje { get; set; }
    }

    public class ArchivosCierre
    {
        public int IdCierre { get; set; }
        public string NombreProceso { get; set; }
        public string NombreArchivo { get; set; }
        public string RutaArchivo { get; set; }
        public DateTime? FecUltimaCarga { get; set; }
    }
}
